#include <iostream>
#include <string>

class Notification {
    public:
        void setTitle(const std::string &title);
        void setBody(const std::string &body);
        void setImage(const std::string &src);

        std::string getTitle() const;
        std::string getBody() const;
        std::string getImgSrc() const;
        
    private:
        std::string m_Title;
        std::string m_Body;
        std::string m_ImgSrc;
};

void Notification::setTitle(const std::string &title)
{
    m_Title = title;
}

void Notification::setBody(const std::string &body)
{
    m_Body = body;
}

void Notification::setImage(const std::string &src)
{
    m_ImgSrc = src;
}

std::string Notification::getTitle() const
{
    return m_Title;
}

std::string Notification::getBody() const
{
    return m_Body;
}

std::string Notification::getImgSrc() const
{
    return m_ImgSrc;
}

class NotificationBuilder {
    public:
        virtual NotificationBuilder &setContentTitle(const std::string &title) = 0;
        virtual NotificationBuilder &setContentBody(const std::string &body) = 0;
        virtual NotificationBuilder &setImage(const std::string &src) = 0;
        virtual Notification build() const = 0;
        virtual void reset() = 0;

    protected:
        Notification notification;
};

class PopUpNotificationBuilder : public NotificationBuilder {
    public:
        NotificationBuilder &setContentTitle(const std::string &title) override;
        NotificationBuilder &setContentBody(const std::string &body) override;
        NotificationBuilder &setImage(const std::string &src) override;
        Notification build() const override;
        void reset() override;
};

NotificationBuilder &PopUpNotificationBuilder::setContentTitle(const std::string &title) 
{
    notification.setTitle(title);  
    return *this; 
}

NotificationBuilder &PopUpNotificationBuilder::setContentBody(const std::string &body)
{
    notification.setBody(body);
    return *this;
}

NotificationBuilder &PopUpNotificationBuilder::setImage(const std::string &src)
{
    notification.setImage(src);
    return *this;
}

Notification PopUpNotificationBuilder::build() const
{
    return notification;
}

void PopUpNotificationBuilder::reset()
{
    notification.setTitle("");
    notification.setBody("");
    notification.setImage("");
}

int main()
{
    NotificationBuilder *notificationBuilder = new PopUpNotificationBuilder();

    Notification myNotification = notificationBuilder->setContentTitle("My Notification")
                                                      .setContentBody("Some Body")
                                                      .setImage("Some source")
                                                      .build();

    std::cout << "Notification build: \n" <<
              myNotification.getTitle() << "\n" << 
              myNotification.getBody() << "\n" <<
              myNotification.getImgSrc() << "\n";

    return 0;
}