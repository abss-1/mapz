#include <iostream>
#include <string>

class Socket {
    public:
        virtual std::string osInUse() const = 0;
};

class UnixSocket : public Socket {
    public:
        std::string osInUse() const override;
};

std::string UnixSocket::osInUse() const
{
    return "Unix Socket";
}

class WinSocket : public Socket {
    public:
        std::string osInUse() const override;
};

std::string WinSocket::osInUse() const
{
    return "Windows Socket";
}

class Thread {
    public:
        virtual std::string osInUse() const = 0;
};

class UnixThread : public Thread {
    public:
        std::string osInUse() const override;
};

std::string UnixThread::osInUse() const
{
    return "Unix Thread";
}

class WinThread : public Thread {
    public:
        std::string osInUse() const override;
};

std::string WinThread::osInUse() const
{
    return "Windows Thread";
}

class AbstractFactory {
    public: 
        virtual Socket *getSocket() const = 0;
        virtual Thread *getThread() const = 0;
};

class UnixFactory : public AbstractFactory {
    public: 
        Socket *getSocket() const override;
        Thread *getThread() const override;
};

Socket *UnixFactory::getSocket() const
{
    return new  UnixSocket();
}

Thread *UnixFactory::getThread() const
{
    return new UnixThread();
}

class WinFactory : public AbstractFactory {
    public: 
        Socket *getSocket() const override;
        Thread *getThread() const override;
};

Socket *WinFactory::getSocket() const
{
    return new  WinSocket();
}

Thread *WinFactory::getThread() const
{
    return new WinThread();
}

Thread *createThread(const AbstractFactory * const osDepImplFactory)
{
    return osDepImplFactory->getThread();
}

Socket *createSocket(const AbstractFactory * const osDepImplFactory)
{
    return osDepImplFactory->getSocket();
}

int main()
{
    AbstractFactory *osDepComponentsFactory = nullptr;

    #ifdef __linux__
        osDepComponentsFactory = new UnixFactory();
    #elif _WIN32
        osDepComponentsFactory = new WinFactory();
    #endif

    std::cout << createSocket(osDepComponentsFactory)->osInUse() << "\n";
    std::cout << createThread(osDepComponentsFactory)->osInUse() << "\n";

    return 0;
}