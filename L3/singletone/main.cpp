#include <iostream>
#include <string>
#include <mutex>
#include <map>

class DBManager {
    public:
        static  DBManager& getInstance();

        DBManager(const DBManager &other) = delete;
        DBManager *operator=(const DBManager &other) = delete;

        void createConnection(const std::string &dbName, const std::string &connectionName);
        int getConnection(const std::string &connectionName) const;

    private:
        DBManager() = default;
        ~DBManager();

    private:
        static std::mutex m_Mutex;
        static DBManager *m_Instance;

        std::map<std::string, int> m_Connections;
};

DBManager *DBManager::m_Instance = nullptr;
std::mutex DBManager::m_Mutex;

DBManager::~DBManager()
{
    delete m_Instance;
}

DBManager &DBManager::getInstance()
{
    std::lock_guard<std::mutex> lock(m_Mutex);

    if (!m_Instance)
    {
        m_Instance = new DBManager();
    }

    return *m_Instance;
}

void DBManager::createConnection(const std::string &dbName, const std::string &connectionName)
{
    if (!m_Connections.count(connectionName))
    {
        m_Connections[connectionName] = rand() % 10; 
    }
}

int DBManager::getConnection(const std::string &connectionName) const
{
    if (m_Connections.count(connectionName))
    {
        return m_Connections.at(connectionName);
    }

    return -1;
}

static const std::string songsDbConnection = "sngcon";
static const std::string playlistDbConnection = "plscon";

int main()
{
    DBManager &dbManager = DBManager::getInstance();

    dbManager.createConnection("someDB", songsDbConnection);

    std::cout << "Connection desc for songs conenction: " << dbManager.getConnection(songsDbConnection) << "\n";

    DBManager &dbManager2 = DBManager::getInstance();

    std::cout << "Connection desc for songs conenction: " << dbManager2.getConnection(songsDbConnection) << "\n";
}