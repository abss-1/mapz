#include <iostream>

class Notifier {
    public:
        virtual ~Notifier() {};

        virtual bool send(const std::string &) = 0;
};

class NotifierDecorator : public Notifier {
    public:
        bool send(const std::string &toSend) override
        {
            if (wrappie)
            {
                return wrappie->send(toSend);
            }
            return false;
        }

        void setNotifier(Notifier *other)
        {
            wrappie = other;
        }

    protected:
        Notifier *wrappie = nullptr;
};

class SlackNotifier : public NotifierDecorator {
    public:
        bool send(const std::string &toSend) override
        {
            std::cout << "Slack notifier sent: " << toSend << "\n";
            return NotifierDecorator::send(toSend);
        }
};

class EmailNotifier : public  NotifierDecorator {
    public:
        bool send(const std::string &toSend) override
        {
            std::cout << "Email notifier sent: " << toSend << "\n";
            return NotifierDecorator::send(toSend);
        }
};

class DesktopNotifier : public NotifierDecorator {
    public:
        bool send(const std::string &toSend) override
        {
            std::cout << "Desktop notifier sent: " << toSend << "\n";
            return NotifierDecorator::send(toSend);
        }
};

int main()
{
    NotifierDecorator *multiNotifier = new SlackNotifier();
    NotifierDecorator *emailNotifier = new EmailNotifier();
    NotifierDecorator *desktopNotifier = new DesktopNotifier();

    multiNotifier->setNotifier(emailNotifier);
    emailNotifier->setNotifier(desktopNotifier);

    multiNotifier->send("That is time to complete grocery task!");
}