#include <iostream>
#include <functional>
#include <algorithm>
#include <list>

struct Time
{
    enum TimeMeasures {
        Days = 0,
        Hours,
        Minutes,
        Count 
    };

    static std::string measureStringify(TimeMeasures measure)
    {
        std::string measureStringify;
        switch (measure)
        {
        case Days: measureStringify = "days"; 
            break;
        case Hours: measureStringify = "hours"; 
            break;
        case Minutes: measureStringify = "minutes"; 
            break;
        default:
            break;
        }

        return measureStringify;
    }

    unsigned char days = 0;
    unsigned char hours = 0;
    unsigned char minutes = 0;
};

std::ostream &operator<<(std::ostream &out, const Time &time)
{
    unsigned char *timeptr = (unsigned char *)&time;

    out << time.hours << " ";
    
    for (unsigned i = 0; i < Time::TimeMeasures::Count; ++i)
    {
        if (!(int)(*timeptr)) {
            break;
        }
        
        out << (int)(*timeptr) << " " << Time::measureStringify((Time::TimeMeasures)i) << " ";
        ++timeptr;
    }

    return out;
}


class TaskComponent {
    public:
        virtual ~TaskComponent() {};
        virtual TaskComponent& add(TaskComponent *taskComponent) { return *this; }
        virtual void removeLast() {};
        virtual void removeIf(std::function<bool(TaskComponent *taskComponent)> pred) {};

        void setParent(TaskComponent *parent) {};
        virtual void operation() const {};

        virtual bool hasChildren() const { return false; };

    protected:
        TaskComponent *parent = nullptr;
};

class Task : public TaskComponent { 
    public:
        Task(std::string title, Time _timeToComplete)
            : taskTitle { title }
            , timeToComplete { _timeToComplete }
        {

        }

        void operation() const override
        {
            std::cout << taskTitle << " --- " << timeToComplete << " left\n";
            for (auto &child : children)
            {
                child->operation();
            }
        }

        Task& add(TaskComponent *child) override
        {
            if (child)
            {
                children.push_back(child);
                child->setParent(this);
            }

            return *this;
        }

        void removeLast() override
        {
            children.erase(children.end());
        }

        void removeIf(std::function<bool(TaskComponent *taskComponent)> pred) 
        {
            auto newEnd = std::remove_if(children.begin(), children.end(), pred);

            children.erase(newEnd, children.end());
        }

        void setParent(TaskComponent *parent)
        {
            if (parent)
            {
                this->parent = parent;
            }
        }

        bool hasChildren() const override
        {
            return children.empty();
        }

        const std::list<TaskComponent *>& getChildren() const
        {
            return children;
        }

    private:
        std::list<TaskComponent *> children;
        std::string taskTitle;
        Time timeToComplete;
}; 

class LeafComponent : public TaskComponent {
    public: 
};

void printTask(TaskComponent *taskComponent)
{
}

int main()
{
    TaskComponent *groceryTask = new Task("Shop grocery", {1, 23, 34} ),
                  *buyPotatoesTask = new Task("Buy potatoes", {1}),
                  *buyMilkTask = new Task("Buy milk", {1}),
                  *buyOrangesTask = new Task("Buy oranges", {1}),
                  *buyCandiesTask = new Task("Buy candies", {1}),
                  *leaf1 = new LeafComponent(),
                  *leaf2 = new LeafComponent(),
                  *leaf3 = new LeafComponent(),
                  *leaf4 = new LeafComponent();

    groceryTask->add(buyPotatoesTask).add(buyMilkTask).add(buyOrangesTask).add(buyCandiesTask);

    buyPotatoesTask->add(leaf1);
    buyMilkTask->add(leaf2);
    buyOrangesTask->add(leaf3);
    buyCandiesTask->add(leaf4);

    Task task = *(Task *)groceryTask;

    groceryTask->operation();
}