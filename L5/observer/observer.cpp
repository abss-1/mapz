#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

class User {
    public:
        virtual void receiveNotification() const = 0;
};

class SomeUser : public User {
    public: 
        void receiveNotification() const
        {
            std::cout << "News received!\n";
        }
};

class NewsPublisher {
    public:
        std::string subscribe(User *user)
        {
            std::string unsubHash = std::move(generateUnsubHash());
            m_Subs.push_back( {unsubHash, user} );

            return unsubHash;
        }

        void unsubscribe(const std::string &hash)
        {
            auto sub = std::find_if(m_Subs.begin(), m_Subs.end(), [&hash](const std::pair<std::string, User*> &sub){
                return sub.first == hash;
            });

            if (sub != m_Subs.end())
            {
                m_Subs.erase(sub);
            }
        }

        void notifySubscribers() const
        {
            for (const auto &sub : m_Subs)
                sub.second->receiveNotification();
        }   

    private:
        std::string generateUnsubHash()
        {
            std::string hash; 
            for (int i = 0; i < m_HashLength; ++i)
                hash[i] = rand() % 255;

            return hash;
        }

        const static unsigned int m_HashLength = 10;
        std::vector<std::pair<std::string, User*>> m_Subs;
};

int main ()
{
    NewsPublisher newsPublisher;
    std::vector<std::pair<std::string, SomeUser>> users(10);

    for (int i = 0; i < 10; ++i)
        users[i].first = newsPublisher.subscribe(&users[i].second);

    std::cout << "First notification wave: \n";
    newsPublisher.notifySubscribers();

    for (int i = 0; i < 10; ++i)
    {
        if (i % 2 == 0)
        {
            newsPublisher.unsubscribe(users[i].first);
        }
    }

    std::cout << "\n\n\nSecond notification wave: \n";
    newsPublisher.notifySubscribers();

    return 0;
}