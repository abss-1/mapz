#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <map>

class UsersModel {
    public:
        enum UserMode {
            NONE = 0,
            NUSER,
            ADMIN
        };

        UsersModel()
            : m_CurrentUser { NONE, "" }
        {
            setMockData();
        }

        bool isUserLoggedIn(const std::string &username, const std::string &password) const
        {
            for (auto &users : m_UserPass)
            {
               auto user = users.second.find(username);
                
                if (user != users.second.end())
                {
                     if ((*user).second == password)
                    {
                        m_CurrentUser = { users.first, username };
                        return true;    
                    }
                }
            }
            return false;
        }

        std::pair<UserMode, std::string> getUserInfo()
        {
            return m_CurrentUser;
        }

    private:
        void setMockData() {
            m_UserPass.insert( { NUSER, std::map<std::string, std::string>() } );
            m_UserPass.insert( { ADMIN, std::map<std::string, std::string>() } );

            m_UserPass.find(NUSER)->second.insert({"Tom", "123"});
            m_UserPass.find(ADMIN)->second.insert({"Jack", "admin"});
        }

    private:
        std::multimap<UserMode, std::map<std::string, std::string> > m_UserPass;
        mutable std::pair<UserMode, std::string> m_CurrentUser;
};

class ViewRenderer {
    public:
        virtual void render() const
        {
           system("clear");
        }    
};

class UserViewRenderer : public ViewRenderer {
    public:
        void render() const override
        {
            ViewRenderer::render();
            std::cout << "Normal user interface\n";
        }
};

class AdminViewRenderer : public ViewRenderer {
    public:
        void render() const override
        {
            ViewRenderer::render();
            std::cout << "Admin interface\n";
        }
};

class LoginController {
    public:
        LoginController()
            : m_UserModel { new UsersModel() }
        {

        }

        void enterUsername() const
        {
            std::cin >> m_UsernameOnCheck;
        }

        void enterPassword() const
        {
            std::cin >> m_PasswordOnCheck;
            
        }

        const ViewRenderer * getNewRenderer(const ViewRenderer * defaultRenderer)
        {
            if (m_UserModel->isUserLoggedIn(m_UsernameOnCheck, m_PasswordOnCheck))
            {
                auto userInfo = m_UserModel->getUserInfo();

                if (userInfo.first == UsersModel::ADMIN)
                {
                    return new AdminViewRenderer();
                }
                else if (userInfo.first == UsersModel::NUSER)
                {
                    return new UserViewRenderer();
                }
           }
            return defaultRenderer;
        }

    private: 
        UsersModel *m_UserModel;
        mutable std::string m_UsernameOnCheck;
        mutable std::string m_PasswordOnCheck;
};

class App {
    public: 
        App();
        void changeViewRenderer(const ViewRenderer *newViewRenderer);

    private:
        const ViewRenderer *m_ViewRenderer = nullptr;
};

class LoginView : public ViewRenderer {
    public:
        LoginView(App *app)
            : m_App(app)
        {
            m_LoginController = new LoginController();
        }

        void render() const override
        {
            ViewRenderer::render();
            std::cout << "=======LOGIN=======\n";
            std::cout << "Username: ";
            m_LoginController->enterUsername();
            std::cout << "Password: ";
            m_LoginController->enterPassword();

            m_App->changeViewRenderer(m_LoginController->getNewRenderer(this));
        }

    private:
        App *m_App;
        LoginController *m_LoginController;
};

App::App()
{
    m_ViewRenderer = new LoginView(this);
    m_ViewRenderer->render();
}

void App::changeViewRenderer(const ViewRenderer *newViewRenderer)
{
    // if (m_ViewRenderer)
    // {
    //     delete m_ViewRenderer;
    // }

    m_ViewRenderer = newViewRenderer;
    m_ViewRenderer->render();
} 

int main()
{
    App *myApp = new App();

    return 0;
}