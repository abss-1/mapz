#include <vector>
#include <string>
#include <algorithm>


class Notifier;
using NotificationWrapper = std::vector<std::pair<unsigned, std::pair<Notifier *, std::string>>>;

namespace queue_regulator_algorithms {

class SendingStrategy {
    public:
        virtual NotificationWrapper::const_iterator getNextNotification(const NotificationWrapper &notifications) const = 0;
};

class FIFO : public SendingStrategy {
    NotificationWrapper::const_iterator getNextNotification(const NotificationWrapper &notifications) const override
    {
        return notifications.cbegin();
    }
};

class LowerPriorityFirst : public SendingStrategy {
    NotificationWrapper::const_iterator getNextNotification(const NotificationWrapper &notifications) const override
    {
        NotificationWrapper::const_iterator citerator = std::min_element(notifications.begin(), notifications.end(), 
            [](const std::pair<unsigned, std::pair<Notifier *, std::string>> &a, 
               const std::pair<unsigned, std::pair<Notifier *, std::string>> &b){
                return a.first < b.first; 
        });

        return citerator;
    }
};

}
