#include <iostream>
#include <unistd.h>
#include <string>
#include <thread>
#include <vector>
#include "strategy.cpp"

// g++ -std=c++17 facade.cpp strategy.cpp -pthread

class Notifier {
    public:
        virtual ~Notifier() {};

        virtual bool send(const std::string &) = 0;

        virtual void countItself(unsigned &count) = 0;
};

class NotifierDecorator : public Notifier {
    public:
        bool send(const std::string &toSend) override
        {
            if (wrappie)
            {
                return wrappie->send(toSend);
            }
            return true;
        }

        void countItself(unsigned &count)
        {
            ++count;
            if (wrappie)
            {
                return wrappie->countItself(count);
            }
        }

        void setNotifier(Notifier *other)
        {
            wrappie = other;
        }

    protected:
        Notifier *wrappie = nullptr;
};

class SlackNotifier : public NotifierDecorator {
    public:
        bool send(const std::string &toSend) override
        {
            std::cout << "Slack notifier sent: " << toSend << "\n";
            return NotifierDecorator::send(toSend);
        }
};

class EmailNotifier : public  NotifierDecorator {
    public:
        bool send(const std::string &toSend) override
        {
            std::cout << "Email notifier sent: " << toSend << "\n";
            return NotifierDecorator::send(toSend);
        }
};

class DesktopNotifier : public NotifierDecorator {
    public:
        bool send(const std::string &toSend) override
        {
            std::cout << "Desktop notifier sent: " << toSend << "\n";
            return NotifierDecorator::send(toSend);
        }
};

class Notification {
    public:
        void setTitle(const std::string &title);
        void setBody(const std::string &body);
        void setImage(const std::string &src);

        std::string getTitle() const;
        std::string getBody() const;
        std::string getImgSrc() const;
        
    private:
        std::string m_Title;
        std::string m_Body;
        std::string m_ImgSrc;
};

void Notification::setTitle(const std::string &title)
{
    m_Title = title;
}

void Notification::setBody(const std::string &body)
{
    m_Body = body;
}

void Notification::setImage(const std::string &src)
{
    m_ImgSrc = src;
}

std::string Notification::getTitle() const
{
    return m_Title;
}

std::string Notification::getBody() const
{
    return m_Body;
}

std::string Notification::getImgSrc() const
{
    return m_ImgSrc;
}

class NotificationBuilder {
    public:
        virtual NotificationBuilder &setContentTitle(const std::string &title) = 0;
        virtual NotificationBuilder &setContentBody(const std::string &body) = 0;
        virtual NotificationBuilder &setImage(const std::string &src) = 0;
        virtual Notification build() const = 0;
        virtual void reset() = 0;

    protected:
        Notification notification;
};

class PopUpNotificationBuilder : public NotificationBuilder {
    public:
        NotificationBuilder &setContentTitle(const std::string &title) override;
        NotificationBuilder &setContentBody(const std::string &body) override;
        NotificationBuilder &setImage(const std::string &src) override;
        Notification build() const override;
        void reset() override;
};

NotificationBuilder &PopUpNotificationBuilder::setContentTitle(const std::string &title) 
{
    notification.setTitle(title);  
    return *this; 
}

NotificationBuilder &PopUpNotificationBuilder::setContentBody(const std::string &body)
{
    notification.setBody(body);
    return *this;
}

NotificationBuilder &PopUpNotificationBuilder::setImage(const std::string &src)
{
    notification.setImage(src);
    return *this;
}

Notification PopUpNotificationBuilder::build() const
{
    return notification;
}

void PopUpNotificationBuilder::reset()
{
    notification.setTitle("");
    notification.setBody("");
    notification.setImage("");
}

class NotificationPrioritySetter;
class NotificationSenderFacade;

class NotificationQueueRegulator {
    private:
        static queue_regulator_algorithms::SendingStrategy *sendingStrategy;

        static NotificationWrapper::const_iterator
            getNextToSend(const NotificationWrapper &notificationsQueue);

        static void setStrategy(queue_regulator_algorithms::SendingStrategy *strategy)
        {
            if (strategy)
            {
                if (sendingStrategy)
                {
                    delete sendingStrategy;
                }

                sendingStrategy = strategy;
            }
        }
            
    friend NotificationSenderFacade;
};

class NotificationPrioritySetter {
    public:
        static unsigned getPriorityFor(const std::pair<Notifier *, const std::string &> &notificationKit);

    private:
        static unsigned getPriorityValue(unsigned canalsNumber, unsigned notificationLenght);
};

class NotificationSenderFacade {
    public:
        NotificationSenderFacade()
        {
            sendingThread = new std::thread(&NotificationSenderFacade::sendWorker, this);
            NotificationQueueRegulator::setStrategy(new queue_regulator_algorithms::FIFO());
        }

        ~NotificationSenderFacade()
        {
            sendingThread->join();
        }

        void send(Notifier *notifierEngine, const std::string &notification);

    private:
        void sendWorker();

        static std::vector<std::pair<unsigned, std::pair<Notifier *, std::string>>> sendingQueue;
        static std::thread *sendingThread;

    friend NotificationQueueRegulator;
};

std::vector<std::pair<unsigned, std::pair<Notifier *, std::string>>> NotificationSenderFacade::sendingQueue;
std::thread *NotificationSenderFacade::sendingThread;

queue_regulator_algorithms::SendingStrategy *NotificationQueueRegulator::sendingStrategy;

void NotificationSenderFacade::sendWorker()
{
    while (true)
    {
        std::cerr << "Done: " << sendingQueue.size() << "\n";
        if (!sendingQueue.empty())
        {
            auto notificationToSend = NotificationQueueRegulator::getNextToSend(sendingQueue);

            auto [notifierEngine, notification] = (*notificationToSend).second;

            if ( notifierEngine->send(notification) )
            {
                std::cout << std::flush;
                sendingQueue.erase(notificationToSend);
            }
        }

        sleep(1);
    }
    std::cerr << "Done";
}

void NotificationSenderFacade::send(Notifier *notifierEngine, const std::string &notification)
{
    unsigned priorityValue = 
                    NotificationPrioritySetter::getPriorityFor(std::make_pair(notifierEngine, notification));

    sendingQueue.emplace_back(std::make_pair(priorityValue, std::make_pair(notifierEngine, notification)));
}

unsigned NotificationPrioritySetter::getPriorityFor(const std::pair<Notifier *, const std::string &> &notificationKit)
{
    auto [notifierEngine, notification] = notificationKit;

    unsigned numberOfSendCanals = 0;
    notifierEngine->countItself(numberOfSendCanals);

    return getPriorityValue(numberOfSendCanals, notification.size());
}

unsigned NotificationPrioritySetter::getPriorityValue(unsigned canalsNumber, unsigned notificationLenght)
{   
    return (canalsNumber * 3) + notificationLenght;
}

NotificationWrapper::const_iterator 
            NotificationQueueRegulator::getNextToSend(const NotificationWrapper &notificationsQueue)
{
    return sendingStrategy->getNextNotification(notificationsQueue);
}

int main()
{
    std::string mess1 = "Hello Slack, Desktop, Email",
                mess2 = "Hello Desktop, Email",
                mess3 = "Hello Email";

    NotifierDecorator *slackDesktopEmail = new SlackNotifier(),
                      *desktopEmail = new DesktopNotifier(),
                      *email = new EmailNotifier();

    desktopEmail->setNotifier(email);
    slackDesktopEmail->setNotifier(desktopEmail);


    NotificationSenderFacade sender;
    sender.send(slackDesktopEmail, mess1);
    sender.send(desktopEmail, mess2);
    sender.send(email, mess3);
}